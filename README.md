# mutation 요청
``` javascript
mutation {
   createMessage(input: {
     author: "andy",
     content: "hope!!"
   }) {
     id
     author
   }
 }
 ```
 
# query 요청
``` javascript
{
  getMessage(id: "1") {
    id, content, author
  }
}
```